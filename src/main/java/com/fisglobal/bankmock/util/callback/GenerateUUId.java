package com.fisglobal.bankmock.util.callback;

import java.util.Random;
import java.security.SecureRandom;


/**
 * UUID implementation based on a psuedo-random number generator.
 */
public class GenerateUUId implements Cloneable {
	private byte[] bytes; // UUID bytes


	private static SecureRandom secureRandom;
	private static Object randLock = new Object();


	/**
	 * Sets the default random number generator to use when creating new
	 * UUID's. This method must be called before any UUID's are created
	 * and may only be called once.
	 */
	public static void setRandom(SecureRandom r) {
	synchronized (randLock) {
		if (secureRandom != null) {
		throw new IllegalStateException(
			"Random number generator already defined");
		}
		secureRandom = r;
	}
	}


	/**
	 * Creates a new UUID using the default random number generator.
	 * If a default random number generator has not already been set
	 * then a new instance of SecureRandom will be used and set as
	 * the default.
	 */
	public GenerateUUId() {
	if (secureRandom == null) {
		synchronized (randLock) {
			secureRandom = new SecureRandom();
		}
	}
	bytes = createBytes(secureRandom);
	}


	/**
	 * Creates a new UUID using the specified pseudo-random number generator.
	 */
	public GenerateUUId(Random random) {
	bytes = createBytes(secureRandom);
	}


	/**
	 * Creates a new UUID parse from the specified string.
	 */
	public GenerateUUId(String s) {
	bytes = parse(s);
	}


	/**
	 * Returns the string specification of this UUID.
	 */
	public String toString() {
	StringBuffer sb = new StringBuffer(36);
	appendHex(sb, bytes, 0, 4);  // time_low
	//sb.append('-');
	appendHex(sb, bytes, 4, 2);  // time_mid
	//sb.append('-');
	appendHex(sb, bytes, 6, 2);  // time_high_and_version
	//sb.append('-');
	appendHex(sb, bytes, 8, 2);  // clock_seq_and_reserved clock_seq_low
	//sb.append('-');
	appendHex(sb, bytes, 10, 6); // node
	return sb.toString();
	}


	/**
	 * Returns the hash code value for this UUID.
	 */
	public int hashCode() {
	byte[] b = bytes;
	return (((b[12] & 0xff) << 24) |
				((b[13] & 0xff) << 16) |
				((b[14] & 0xff) <<  8) |
				((b[15] & 0xff) <<  0));
	}


	/**
	 * Returns true if this UUID is equal to the specified object.
	 */
	public boolean equals(Object obj) {
	if (!(obj instanceof GenerateUUId)) {
		return false;
	}
	byte[] b1 = this.bytes;
	byte[] b2 = ((GenerateUUId)obj).bytes;
	if (b1.length != b2.length) {
		return false;
	}
	for (int i = 0; i < b1.length; i++) {
		if (b1[i] != b2[i]) {
		return false;
		}
	}
	return true;
	}


	// Appends hex bytes to a string buffer
	private static void appendHex(StringBuffer sb, byte[] b, int off, int len) {
	while (--len >= 0) {
		int n = b[off++] & 0xff;
		sb.append(Character.forDigit(n >> 4, 16));
		sb.append(Character.forDigit(n & 15, 16));
	}
	}


	// Parses UUID specification from a string
	private static byte[] parse(String s) {
	if (s.length() != 36 || s.charAt(8) != '-' || s.charAt(13) != '-'
		|| s.charAt(18) != '-' || s.charAt(23) != '-') {
		throw new IllegalArgumentException();
	}
	byte[] b = new byte[16];
	parseHex(s, 0, b, 0, 4);     // time_low
	parseHex(s, 9, b, 4, 2);     // time_mid
	parseHex(s, 14, b, 6, 2);    // time_high_and_version
	parseHex(s, 19, b, 8, 2);    // clock_seq_and_reserved clock_seq_low
	parseHex(s, 24, b, 10, 6);    // node
	return b;
	}


	// Parses hex bytes from a string
	private static void parseHex(String s, int i, byte[] b, int off, int len) {
	while (--len >= 0) {
		int hi = Character.digit(s.charAt(i++), 16);
		int lo = Character.digit(s.charAt(i++), 16);
		if (hi == -1 || lo == -1) {
		throw new IllegalArgumentException();
		}
		b[off++] = (byte)(hi << 4 | lo);
	}
	}


	private static byte[] createBytes(SecureRandom secureRandom) {
	byte[] b = new byte[16];
	secureRandom.nextBytes(b);
	// Set bits 12-15 of time_hi_and_version to version 4 (indicates
		// that this UUID was generated from a secureRandom number).
	b[6] = (byte)((b[6] & 0x0f) | 0x40);
	// Set bits 6 and 7 of clock_seq_hi_and_reserved to 0 and 1,
	// respectively (indicates IETF variant).
	b[8] = (byte)((b[8] & 0x3f) | 0x80);
	return b;
	}
}
