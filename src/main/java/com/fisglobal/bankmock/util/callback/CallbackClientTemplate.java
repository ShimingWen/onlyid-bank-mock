package com.fisglobal.bankmock.util.callback;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CallbackClientTemplate implements ResourceLoaderAware {
	@Autowired
	private Environment env;
	private ResourceLoader resourceLoader;

	private static final Logger LOG = LoggerFactory
			.getLogger(CallbackClientTemplate.class);

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public RestTemplate callbackClientRestTemplate() throws Exception {
		SSLContext sslContext = SSLContexts
				.custom()
				.loadKeyMaterial(
						getStore(env.getProperty("callback.ssl.keystore"), env
								.getProperty("callback.ssl.keystorepwd")
								.toCharArray()),
						env.getProperty("callback.ssl.keypwd")
								.toCharArray())
				.loadTrustMaterial(
						getStore(env.getProperty("callback.ssl.truststore"),
								env.getProperty("callback.ssl.truststorepwd")
										.toCharArray()),
						new TrustSelfSignedStrategy()).useProtocol("TLS")
				.build();
		
		RestTemplate template = getRestTemplateForHTTPS(sslContext);
		template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		return template;

	}

	private RestTemplate getRestTemplateForHTTPS(SSLContext sslContext) {

		SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(
				sslContext, NoopHostnameVerifier.INSTANCE);
		Builder requestConfigBuilder = RequestConfig.custom();
		requestConfigBuilder.setSocketTimeout(Integer.parseInt(env.getProperty("callback.socket.timeout")));

		RequestConfig config = requestConfigBuilder.build();

		CloseableHttpClient closeableHttpClient = HttpClientBuilder.create()
				.setSSLSocketFactory(connectionFactory)
				.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
				.setDefaultRequestConfig(config).build();

		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory(
				closeableHttpClient);
		httpRequestFactory.setHttpClient(closeableHttpClient);

		return new RestTemplate(httpRequestFactory);
	}

	private KeyStore getStore(final String storeFileName, final char[] password)
			throws KeyStoreException, IOException, CertificateException,
			NoSuchAlgorithmException {
		final KeyStore store = KeyStore.getInstance("jks");
		InputStream inputStream = null;
		try {
			inputStream = getResource(storeFileName).getInputStream();
			store.load(inputStream, password);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LOG.error("Error occured at closing the stream", e);
				}
			}
		}
		return store;
	}

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	private Resource getResource(String location) {
		Resource resource = this.resourceLoader.getResource(location);
		System.out.println(resource.getFilename());
		return resource;
	}

}
