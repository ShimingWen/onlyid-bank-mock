package com.fisglobal.bankmock.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class IdPAuthenticationFilter extends GenericFilterBean {

	private static final Logger LOG = LoggerFactory.getLogger(IdPAuthenticationFilter.class);

	protected JWTAuthenticationProvider authenticationManager;

	public IdPAuthenticationFilter(JWTAuthenticationProvider tAuthenticationManager) {
		authenticationManager = tAuthenticationManager;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String id_token = httpRequest.getParameter("id_token");
		LOG.debug("*******In IdPAuthenticationFilter*******");
		if (id_token == null) {
			LOG.error("id_tokenis NULL - " + httpRequest.getRequestURI());
		}

		if (null != id_token) {
			LOG.debug("*****id_token flow**************");
			JWTAuthentication token = new JWTAuthentication(id_token);
			// Make sure we're authenticated
			try {
				Authentication auth = authenticationManager.authenticate(token);
				SecurityContextHolder.getContext().setAuthentication(auth);
			} catch (AuthenticationException ex) {
				LOG.error("AuthenticationException occurred: " + ex.getMessage());
				SecurityContextHolder.getContext().setAuthentication(null);
			}

			chain.doFilter(request, response);

		} else {
			if ((SecurityContextHolder.getContext().getAuthentication() != null)
					&& SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
				LOG.debug("SecurityContextHolder.getContext-"
						+ SecurityContextHolder.getContext().getAuthentication().getName() + " | SessionID:"
						+ httpRequest.getSession().getId());
				chain.doFilter(request, response);
			} else {
				SecurityContextHolder.getContext().setAuthentication(null);
				SecurityContextHolder.clearContext();
				LOG.error("No Authentication exists - id_token or preauthentication session");
				httpResponse.sendRedirect("/sessionExpired");
			}
		}
	}
}
