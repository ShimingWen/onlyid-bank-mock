package com.fisglobal.bankmock.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;


public class JWTAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private Environment env;

    protected String secret;

    public JWTAuthenticationProvider() {

    }

    private static final Logger LOG = LoggerFactory.getLogger(JWTAuthenticationProvider.class);

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	JWTAuthentication jwtAuth = (JWTAuthentication)authentication;
	Jws<Claims> jws;
	try {
	    CertificateFactory f = CertificateFactory.getInstance("X.509");
	    String idPPublicCert = env.getProperty("IdP.Public.Cert");
	    X509Certificate certificate = (X509Certificate)f.generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(idPPublicCert.getBytes("UTF-8"))));
	    PublicKey pk = certificate.getPublicKey();
	    String issuerId = env.getProperty("issuerId");
	    jws = Jwts.parser()
		    .requireIssuer(issuerId)
		    .setSigningKey(pk)
		    .parseClaimsJws(jwtAuth.getToken());
	} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException |
		IllegalArgumentException | CertificateException | UnsupportedEncodingException ex) {
	    ex.printStackTrace();
	    throw new BadCredentialsException("The token is not valid");
	}
	Date checkDate = Date.from(Instant.now());
	Date expirDate = jws.getBody().getExpiration();
	if (null == expirDate || checkDate.after(expirDate) ) {
	    throw new BadCredentialsException("The token is expired");
	}
	Date notBeforeDate = jws.getBody().getNotBefore();
	if (null == notBeforeDate || checkDate.before(notBeforeDate) ) {
	    throw new BadCredentialsException("The token not before date is invalid");
	}
	jwtAuth.setTokenClaims(jws.getBody());
	jwtAuth.setAuthenticated(true);
	LOG.debug("*****************JWT Token Validation Successful");
	return jwtAuth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
	return JWTAuthentication.class.isAssignableFrom(authentication);
    }

    public String createJWTToken(String username) {
	String issuerId = env.getProperty("issuerId");
	long tokenDurationSeconds = Long.parseLong(env.getProperty("token.duration.seconds"));
	long tokenCreationBufferSeconds = Long.parseLong(env.getProperty("token.creation.buffer.seconds"));
	return Jwts.builder()
		.setSubject(username)
		.setIssuer(issuerId)
		.setIssuedAt(Date.from(Instant.now()))
		.setExpiration(Date.from(Instant.now().plusSeconds(tokenDurationSeconds)))
		.setNotBefore(Date.from(Instant.now().minusSeconds(tokenCreationBufferSeconds)))
		.signWith(SignatureAlgorithm.HS512, secret)
		.compact();
    }
}
