package com.fisglobal.bankmock.security;

import io.jsonwebtoken.Claims;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class JWTAuthentication implements Authentication {

   private static final long serialVersionUID = -2771679103768598775L;

	public static final String ROLES_CLAIM_NAME = "ProfileList";

    protected String tokenString;
    protected Claims tokenClaims;
    protected List<GrantedAuthority> authorities;
    protected boolean authenticated;


    public JWTAuthentication(String tString) {
        tokenString = tString;
        authorities = new ArrayList<>(0);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getDetails() {
        return tokenClaims.toString();
    }

    @Override
    public Object getPrincipal() {
        return tokenClaims.getSubject();
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return tokenClaims.getSubject();
    }

    public String getToken() {
        return tokenString;
    }

    public void setTokenClaims(Claims tTokenClaims) {
        this.tokenClaims = tTokenClaims;
        String roles = tokenClaims.get(ROLES_CLAIM_NAME, String.class);
        if (null != roles) {
        	StringTokenizer strTok = new StringTokenizer(roles, ",");
            ArrayList<GrantedAuthority> authsList = new ArrayList<>(strTok.countTokens());
            while (strTok.hasMoreTokens()) {
                authsList.add(new SimpleGrantedAuthority(strTok.nextToken()));
            }
            authorities = Collections.unmodifiableList(authsList);
        } else {
            authorities = Collections.emptyList();
        }
    }
}