package com.fisglobal.bankmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.fisglobal.bankmock.security.IdPAuthenticationFilter;
import com.fisglobal.bankmock.security.JWTAuthenticationProvider;

@SpringBootApplication
public class BankMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankMockApplication.class, args);
	}

	@Configuration
	@EnableGlobalAuthentication
	protected static class ApplicationSecurity extends WebSecurityConfigurerAdapter {

		@Bean
		public JWTAuthenticationProvider jwtAuthenticationProvider() {
			return new JWTAuthenticationProvider();
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
		    web.ignoring().antMatchers("/mockbank/**", "/health/**", "/sessionExpired", "/enroll/sessionExpired.html", "/enroll/css/style.css", "**/bootstrap.min.css", "**/angular-1.6.1.js");
		}
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable();
			http
				.addFilterBefore(new IdPAuthenticationFilter(jwtAuthenticationProvider()), BasicAuthenticationFilter.class)
				.authorizeRequests()
				.anyRequest().authenticated();
		}

	}

}
