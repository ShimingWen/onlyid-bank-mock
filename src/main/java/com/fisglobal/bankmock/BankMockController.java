package com.fisglobal.bankmock;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BankMockController {
	
	private static final Logger LOG = LoggerFactory.getLogger(BankMockController.class);

	@Autowired
	private Environment env;
	
	@PostMapping("/enroll")
	public String enrollPost() {
		return "redirect:enroll/PIIInput.html";
	}

	@PostMapping("/mockbank/postMe")
	public String redirectFrom(@ModelAttribute("token") String token, RedirectAttributes redirectAttributes)
			throws CertificateException, UnsupportedEncodingException {
		redirectAttributes.addAttribute("token", token);
		return "redirect:success.html";
	}

	@PostMapping("/verifCallback")
	public String redirect(@ModelAttribute("verifCbResp") String verifResp, RedirectAttributes redirectAttributes)
			throws CertificateException, UnsupportedEncodingException {
		LOG.info("call to Bank mock is success");
		redirectAttributes.addAttribute("verifCbResp", verifResp);
		return "redirect:verificationResults.html";
	}

	@GetMapping(value = "/sessionExpired")
	public String sessionExpired(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:enroll/sessionExpired.html";
	}

    @GetMapping(value="/enrollmentPortal")
    public void redirectToLoginPage(HttpServletResponse response) {
    	try {
			response.sendRedirect(env.getProperty("enrollment.login.url"));
		} catch (IOException e) {
			LOG.error("OnlyIDPortalController : redirectToLoginPage() : Exception occurred when redirecting" + e.getMessage());
			e.printStackTrace();
		}
    }

	@PostMapping("/enrolled")
	public String enrollmentSuccess(@ModelAttribute("token") String token, RedirectAttributes redirectAttributes)
			throws CertificateException, UnsupportedEncodingException {
		redirectAttributes.addAttribute("token", token);
		return "redirect:/enroll/enrolled.html";
	}

	@GetMapping("/enrolled")
	public String enrollmentSuccessGET(@ModelAttribute("token") String token, RedirectAttributes redirectAttributes)
			throws CertificateException, UnsupportedEncodingException {
		redirectAttributes.addAttribute("token", token);
		return "redirect:enroll/enrolled.html";
	}

}
