package com.fisglobal.bankmock.common;

public class Constants {
	
	public static final String SYSTEM_ERROR_JSON = "{\"code\":9999,\"message\":\"System exception occurred.  Unable to complete the request.\"}";
	public static final String VALIDATION_ERROR_JSON = "{\"code\":9998,\"message\":\"Invalid input.\"}";
	public static final String UUID = "Uuid";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String SECURITY_TOKEN_TYPE = "Security-Token-Type";
	public static final String APPLICATION_ID = "Application-Id";
	public static final String SOURCE_ID = "Source-Id";


}
