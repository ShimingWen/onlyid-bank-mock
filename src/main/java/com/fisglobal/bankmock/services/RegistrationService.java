/**
 * 
 */
package com.fisglobal.bankmock.services;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fisglobal.bankmock.domain.RegistrationResponse;
import com.fisglobal.bankmock.util.callback.GenerateUUId;


@RestController
public class RegistrationService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    private Environment env;

    @Autowired
    private RestTemplate callbackClientRestTemplate;

    public static final String SYSTEM_ERROR_JSON = "{\"code\":9999,\"message\":\"API exception occurred.  Unable to complete the request.\"}";

    public RegistrationService() {
    }

    @RequestMapping(method = RequestMethod.GET, value = "/mockbank/registrations/{registrationId}", produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> getRegistrationId(@PathVariable String registrationId) {

    GenerateUUId uuid=new GenerateUUId();
	HttpHeaders requestHeaders = new HttpHeaders();
	requestHeaders.set("Uuid", uuid.toString());
	requestHeaders.set("Application-Id", "IDNetwork");
	requestHeaders.set("Source-Id", "191679");
	requestHeaders.set("Security-Token-Type", "Cert");
	requestHeaders.set("Content-Type", "application/json");
	
	try {
	    HttpEntity<?> httpRequest = new HttpEntity<Object>(requestHeaders);
	   
	    String URL= env.getProperty("gemini.Url.get.registration")+ "/" + registrationId;
	    ResponseEntity<RegistrationResponse> resp = callbackClientRestTemplate.exchange(URL, HttpMethod.GET, httpRequest, RegistrationResponse.class);
	
		return new ResponseEntity<>(resp.getBody(), HttpStatus.OK);
	    } catch (HttpStatusCodeException e) {
		String httpStatus = e.getMessage();
		LOG.error("RegistrationService.getRegistrationId  : Error sent from API" + e.getResponseBodyAsString());
		if (httpStatus.trim().equals("400")) {
		    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
		} else {
		    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	    }
    }

}
