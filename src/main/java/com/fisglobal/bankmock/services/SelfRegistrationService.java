package com.fisglobal.bankmock.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fisglobal.bankmock.common.Constants;
import com.fisglobal.bankmock.domain.AdditionalPII;
import com.fisglobal.bankmock.domain.Address;
import com.fisglobal.bankmock.domain.ChannelSignature;
import com.fisglobal.bankmock.domain.Identity;
import com.fisglobal.bankmock.domain.OnBoardingApiRequest;
import com.fisglobal.bankmock.domain.OnBrdRes;
import com.fisglobal.bankmock.domain.PIIInputRequest;
import com.fisglobal.bankmock.domain.RedirectURL;
import com.fisglobal.bankmock.util.callback.GenerateUUId;

@RestController
public class SelfRegistrationService {

	private static final Logger LOG = LoggerFactory.getLogger(SelfRegistrationService.class);

	@Autowired
	private Environment env;

	@Autowired
	private RestTemplate calloutClientRestTemplate;

	public SelfRegistrationService() {
	}

	@RequestMapping(method = RequestMethod.POST, value = "/selfRegistration", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<?> update(@RequestBody PIIInputRequest request) {

		LOG.debug("SelfRegistrationService Starts..");
		HttpHeaders headers = new HttpHeaders();
		GenerateUUId UUID = new GenerateUUId();
		 
		// validate if any
		OnBrdRes onBoardingResponse = null;
		
		

		try {
			
			
			headers.set(Constants.UUID, UUID.toString());
			headers.set(Constants.SOURCE_ID, env.getProperty("selfRegistration.sourceID"));
			headers.set(Constants.SECURITY_TOKEN_TYPE, env.getProperty("selfRegistration.securityToken"));
			headers.set(Constants.APPLICATION_ID, env.getProperty("selfRegistration.applicationID"));
			headers.set(Constants.CONTENT_TYPE, env.getProperty("selfRegistration.contentType"));
			
			Identity identity = new Identity();
			identity.setFirstName(request.getFirstName());
			identity.setMiddleName(request.getMiddleName());
			identity.setLastName(request.getLastName());
			identity.setSsn(request.getSsn());			
			
			Address address = new Address();
			address.setAddressLine1(request.getAddress().getAddressLine1());
			address.setAddressLine2(request.getAddress().getAddressLine2());
			address.setCity(request.getAddress().getCity());
			address.setState(request.getAddress().getState());
			address.setZip(request.getAddress().getZip());
			
			AdditionalPII piiData= new AdditionalPII();
			piiData.setCountry(request.getCountry());
			
			identity.setAddress(address);
			identity.setAdditionalPii(piiData);
			
			OnBoardingApiRequest onBoardingRequest = new OnBoardingApiRequest();
			onBoardingRequest.setIdentity(identity);
			onBoardingRequest.setTransactionType(env.getProperty("selfRegistration.transactionType"));
			onBoardingRequest.setStrategyCode(env.getProperty("selfRegistration.strategyCode"));
			onBoardingRequest.setCustomerId(env.getProperty("selfRegistration.customerID"));

			ChannelSignature channelSignature = new ChannelSignature();
			channelSignature.setDeviceRiskInd(env.getProperty("selfRegistration.deviceRiskInd"));
			onBoardingRequest.setChannelSignature(channelSignature);

			RedirectURL redirectURL = new RedirectURL();
			redirectURL.setType(env.getProperty("selfRegistration.type"));
			redirectURL.setUrl(env.getProperty("selfRegistration.redirectURL")); 
			onBoardingRequest.setRedirectUrl(redirectURL);
			
			onBoardingResponse = calloutClientRestTemplate.postForObject(env.getProperty("selfRegistration.Url"),
					new HttpEntity<Object>(onBoardingRequest, headers), OnBrdRes.class);

			return new ResponseEntity<>(onBoardingResponse, HttpStatus.OK);

		} catch (HttpStatusCodeException e) {
			LOG.error("Error sent from API" + e.getResponseBodyAsString());
			if (e.getResponseBodyAsString().contains("Runtime Error")) {
				return new ResponseEntity<>(Constants.SYSTEM_ERROR_JSON, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			LOG.error("Exception occurred" + e.getMessage());
			e.printStackTrace();
			return new ResponseEntity<>(Constants.SYSTEM_ERROR_JSON, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			LOG.debug("SelfRegistrationService Ends..");
		}
	}

}
