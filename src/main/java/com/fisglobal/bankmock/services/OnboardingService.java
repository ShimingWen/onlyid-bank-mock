/**
 * 
 */
package com.fisglobal.bankmock.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fisglobal.bankmock.domain.OnBoardingRequest;
import com.fisglobal.bankmock.domain.OnBrdRes;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * @author e3013469
 *
 */
@RestController
public class OnboardingService {

    private static final Logger LOG = LoggerFactory.getLogger(OnboardingService.class);

    @Autowired
    private Environment env;

    @Autowired
    private RestTemplate callbackClientRestTemplate;

    public static final String SYSTEM_ERROR_JSON = "{\"code\":9999,\"message\":\"API exception occurred.  Unable to complete the request.\"}";

    public OnboardingService() {
    }

    @RequestMapping(method = RequestMethod.POST, value = "/mockbank/OnBoarding", produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<?> getonBoarding(@RequestBody OnBoardingRequest requestInputs) {

        HttpHeaders requestHeaders = new HttpHeaders();

        // String jsonstring = "{'Content-Type':
        // 'application/json','Application-Id':
        // 'IDNetwork','Security-Token-Type':'Cert','Source-Id':'142888','Uuid':'11213'}";
        JSONObject resobj;
        String mapString = "";
        try {
            resobj = new JSONObject(requestInputs.getRequestHeader());
            Iterator<?> keys = resobj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (key != null) {
                    requestHeaders.set(key, resobj.get(key).toString());
                }
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        try {
            HttpEntity<?> httpRequest = new HttpEntity<Object>(requestInputs.getRequestInputs(), requestHeaders);

            try {
                OnBrdRes resp = callbackClientRestTemplate.postForObject(env.getProperty("gemini.Url"), httpRequest,
                        OnBrdRes.class);
                try {
                    Map<String, Object> claim = new HashMap<String, Object>();
                    claim = (Map<String, Object>) Jwts.parser()
                            .setSigningKey(env.getProperty("gemini.onboarding.secret").getBytes("UTF-8"))
                            .parseClaimsJws(resp.getToken()).getBody();

                    for (Map.Entry<String, Object> entry : claim.entrySet()) {
                        mapString += "Key : " + entry.getKey() + " Value : " + entry.getValue() + ";\r\n";
                    }
                    resp.setMapString(mapString);
                    resp.setPortalUrl(resp.getPortalUrl());
                    resp.setTransactionId(resp.getTransactionId());

                } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException
                        | IllegalArgumentException ex) {
                    ex.printStackTrace();
                    throw new BadCredentialsException("The token is not valid");
                }

                return new ResponseEntity<>(resp, HttpStatus.OK);
            } catch (HttpStatusCodeException e) {
                String httpStatus = e.getMessage();
                LOG.error("OnbordingService.getonBoarding  : Error sent from API" + e.getResponseBodyAsString());
                if (httpStatus.trim().equals("400")) {
                    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
                } else {
                    return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.INTERNAL_SERVER_ERROR);
                }

            }
        } catch (Exception e) {
            LOG.error("OnbordingService.getonBoarding  : Exception occurred: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(SYSTEM_ERROR_JSON, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
