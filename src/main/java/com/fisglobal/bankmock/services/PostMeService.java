/**
 * 
 */
package com.fisglobal.bankmock.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostMeService {

	private static final Logger LOG = LoggerFactory.getLogger(PostMeService.class);

	public PostMeService() {
	}

	@RequestMapping(method = RequestMethod.GET, value = "/mockbank/parseJWT/{token}", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<?> getMBRegistrationId(@PathVariable String token) {

		try {
			if (token != null) {
				String[] split_string = token.split("\\.");
				String base64EncodedHeader = split_string[0];
				String base64EncodedBody = split_string[1];
				String base64EncodedSignature = split_string[2];

				Base64 base64Url = new Base64(true);
				String body = new String(base64Url.decode(base64EncodedBody));

				return new ResponseEntity<>(body, HttpStatus.OK);
			}
		} catch (Exception e) {
			LOG.error("Error in JWT Parsing" + e.getMessage());
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/parseJWT/{token}", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<?> getSelfRegistrationId(@PathVariable String token) {

		try {
			if (token != null) {
				String[] split_string = token.split("\\.");
				String base64EncodedBody = split_string[1];

				Base64 base64Url = new Base64(true);
				String body = new String(base64Url.decode(base64EncodedBody));

				return new ResponseEntity<>(body, HttpStatus.OK);
			}
		} catch (Exception e) {
			LOG.error("Error in JWT Parsing" + e.getMessage());

		}
		return null;
	}

}
