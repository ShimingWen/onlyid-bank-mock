package com.fisglobal.bankmock.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChannelSignature {

    @JsonProperty("deviceRiskInd")
    private String deviceRiskInd = null;

	public String getDeviceRiskInd() {
		return deviceRiskInd;
	}

	public void setDeviceRiskInd(String deviceRiskInd) {
		this.deviceRiskInd = deviceRiskInd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deviceRiskInd == null) ? 0 : deviceRiskInd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChannelSignature other = (ChannelSignature) obj;
		if (deviceRiskInd == null) {
			if (other.deviceRiskInd != null)
				return false;
		} else if (!deviceRiskInd.equals(other.deviceRiskInd))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ChannelSignature [deviceRiskInd=" + deviceRiskInd + "]";
	}

  
}

