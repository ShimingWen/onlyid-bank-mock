package com.fisglobal.bankmock.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

public class AdditionalPII {
  
    @JsonProperty("mobilePhone")
    private String mobilePhone = null;

  
    @JsonProperty("dlState")
    private String dlState = null;

   
    @JsonProperty("dlNumber")
    private String dlNumber = null;

   
    @JsonProperty("emailAddress")
    private String emailAddress = null;

    @JsonProperty("phone")
    private String phone = null;

    
    @JsonProperty("country")
    private String country = null;


	public String getMobilePhone() {
		return mobilePhone;
	}


	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


	public String getDlState() {
		return dlState;
	}


	public void setDlState(String dlState) {
		this.dlState = dlState;
	}


	public String getDlNumber() {
		return dlNumber;
	}


	public void setDlNumber(String dlNumber) {
		this.dlNumber = dlNumber;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((dlNumber == null) ? 0 : dlNumber.hashCode());
		result = prime * result + ((dlState == null) ? 0 : dlState.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((mobilePhone == null) ? 0 : mobilePhone.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdditionalPII other = (AdditionalPII) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (dlNumber == null) {
			if (other.dlNumber != null)
				return false;
		} else if (!dlNumber.equals(other.dlNumber))
			return false;
		if (dlState == null) {
			if (other.dlState != null)
				return false;
		} else if (!dlState.equals(other.dlState))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (mobilePhone == null) {
			if (other.mobilePhone != null)
				return false;
		} else if (!mobilePhone.equals(other.mobilePhone))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "AdditionalPII [mobilePhone=" + mobilePhone + ", dlState=" + dlState + ", dlNumber=" + dlNumber
				+ ", emailAddress=" + emailAddress + ", phone=" + phone + ", country=" + country + "]";
	}

  
}
