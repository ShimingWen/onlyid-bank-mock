package com.fisglobal.bankmock.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnBrdRes {
    
    @JsonProperty("jwtToken")
    private String token;
    
    @JsonProperty("mapString")
    private String mapString;
    
    @JsonProperty("transactionId")
    private String transactionId = null;
    
    @JsonProperty("portalUrl")
    private String portalUrl = null;


	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPortalUrl() {
		return portalUrl;
	}

	public void setPortalUrl(String portalUrl) {
		this.portalUrl = portalUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMapString() {
		return mapString;
	}

	public void setMapString(String mapString) {
		this.mapString = mapString;
	}

	
}
