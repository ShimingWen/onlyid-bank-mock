package com.fisglobal.bankmock.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnBoardingApiRequest {

	@JsonProperty("transactionType")
	private String transactionType = null;

	@JsonProperty("strategyCode")
	private String strategyCode = null;

	@JsonProperty("customerId")
	private String customerId = null;

	@JsonProperty("channelSignature")
	private ChannelSignature channelSignature = null;

	@JsonProperty("identity")
	private Identity identity = null;

	@JsonProperty("redirectUrl")
	private RedirectURL redirectUrl = null;

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getStrategyCode() {
		return strategyCode;
	}

	public void setStrategyCode(String strategyCode) {
		this.strategyCode = strategyCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public ChannelSignature getChannelSignature() {
		return channelSignature;
	}

	public void setChannelSignature(ChannelSignature channelSignature) {
		this.channelSignature = channelSignature;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public RedirectURL getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(RedirectURL redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channelSignature == null) ? 0 : channelSignature.hashCode());
		result = prime * result + ((customerId == null) ? 0 : customerId.hashCode());
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((redirectUrl == null) ? 0 : redirectUrl.hashCode());
		result = prime * result + ((strategyCode == null) ? 0 : strategyCode.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OnBoardingApiRequest other = (OnBoardingApiRequest) obj;
		if (channelSignature == null) {
			if (other.channelSignature != null)
				return false;
		} else if (!channelSignature.equals(other.channelSignature))
			return false;
		if (customerId == null) {
			if (other.customerId != null)
				return false;
		} else if (!customerId.equals(other.customerId))
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (redirectUrl == null) {
			if (other.redirectUrl != null)
				return false;
		} else if (!redirectUrl.equals(other.redirectUrl))
			return false;
		if (strategyCode == null) {
			if (other.strategyCode != null)
				return false;
		} else if (!strategyCode.equals(other.strategyCode))
			return false;
		if (transactionType == null) {
			if (other.transactionType != null)
				return false;
		} else if (!transactionType.equals(other.transactionType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OnBoardingApiRequest [ customerId=" + customerId + ", channelSignature=" + channelSignature + ", identity=" + identity
				+ ", redirectUrl=" + redirectUrl + " transactionType=" + transactionType + ", strategyCode=" + strategyCode
				+ "]";
	}

}
