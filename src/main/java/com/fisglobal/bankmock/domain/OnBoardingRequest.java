package com.fisglobal.bankmock.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnBoardingRequest {
    
    @JsonProperty("requestHeader")
    private String requestHeader;
    
    @JsonProperty("requestInputs")
    private String requestInputs;

	public String getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(String requestHeader) {
		this.requestHeader = requestHeader;
	}

	public String getRequestInputs() {
		return requestInputs;
	}

	public void setRequestInputs(String requestInputs) {
		this.requestInputs = requestInputs;
	}
   
    

}
