package com.fisglobal.bankmock.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationGetResponseReasonCodes {
    
	  @JsonProperty("code")
	  private String code = null;

	  public void setCode(String code) {
		this.code = code;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("description")
	  private String description = null;
	
}
