package com.fisglobal.bankmock.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegistrationResponse {
    
	@JsonProperty("registrationId")
	private String registrationId = null;
	  
    @JsonProperty("digitalId")
    private String digitalId;
    
    @JsonProperty("transactionId")
    private String transactionId;

    @JsonProperty("registered")
    private Boolean registered = null;

    @JsonProperty("status")
    private String status = null;

    @JsonProperty("score")
    private Integer score = null;

    @JsonProperty("overallAssessment")
    private String overallAssessment = null;
    
    @JsonProperty("reasonCodes")
    private List<RegistrationGetResponseReasonCodes> reasonCodes = new ArrayList<RegistrationGetResponseReasonCodes>();

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public void setDigitalId(String digitalId) {
		this.digitalId = digitalId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public void setRegistered(Boolean registered) {
		this.registered = registered;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public void setOverallAssessment(String overallAssessment) {
		this.overallAssessment = overallAssessment;
	}

	public void setReasonCodes(List<RegistrationGetResponseReasonCodes> reasonCodes) {
		this.reasonCodes = reasonCodes;
	}

	
}
