
//Create new Module as onlyDigitalIDApp
var BankMockApp=angular.module('BankMockApp',['ngRoute','ngStorage']);


BankMockApp.controller('selfEnrollmentController',function($scope,$location,$http){
	
	//Regex for the UI Input Validation 
	
	$scope.fName_regex = "[a-zA-Z0-9-\' \\\\.&%+\"/]{0,20}";
	$scope.lName_regex = "[a-zA-Z0-9-\' \\\\.&%+\"/]{0,30}";
	$scope.address_regex = "[a-zA-Z0-9#/\',\\\\. ]{0,99}";
	$scope.city_regex = "[a-zA-Z- \'.]{0,30}";
	$scope.zip_regex5 = "[0-9]{5}";
	$scope.zip_regex4 = "^$|[0-9]{4}";
	$scope.state_regex = "^$|[a-zA-Z]{2}";
	$scope.ssn_regex = "[0-9]{4,9}?|^\\s*$";
	$scope.country_regex ="^$|[a-zA-Z]{2}";
	$scope.data={};	
	$scope.data.address={};
	
		
	$scope.registration=function(theForm)
	{
		$scope.errorMes='';
		theForm.$submitted=true;
		console.log(""+$scope.data);
		
		$scope.data.firstName = $scope.selfRegistrationForm.fName;
		$scope.data.middleName = $scope.selfRegistrationForm.mName;
		$scope.data.lastName = $scope.selfRegistrationForm.lName;
		$scope.data.ssn = $scope.selfRegistrationForm.govtNbr;
		$scope.data.address.addressLine1 = $scope.selfRegistrationForm.addressLine1;
		$scope.data.address.addressLine2 = $scope.selfRegistrationForm.addressLine2;
		$scope.data.address.city = $scope.selfRegistrationForm.city;
		$scope.data.address.state = $scope.selfRegistrationForm.state;
		$scope.data.address.zip = $scope.selfRegistrationForm.zipCode5;
		if($scope.selfRegistrationForm.zipCode4 != undefined) {
			$scope.data.address.zip += $scope.selfRegistrationForm.zipCode4;
		}
		$scope.data.country = $scope.selfRegistrationForm.country;
		
		
		
		if(theForm.$valid){
			
			$http.post("/selfRegistration",$scope.data,{ headers: { 'Content-Type': 'application/json' } }).then(function(response)
					{			
				
						console.log("resp->"+response.data.jwtToken);

						document.redirectselfRegistrationForm.elements.id_token.value=response.data.jwtToken;
						document.redirectselfRegistrationForm.action=response.data.portalUrl;		
						document.redirectselfRegistrationForm.method='post';
						document.redirectselfRegistrationForm.submit();
						
					})
			.catch(function(response)
					{
						$scope.errorMes="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
					})
		}
			
		
		
	}
	

});




BankMockApp.controller('RegistrationController',function($scope,$location,$http,$localStorage){
	
	$scope.results=localStorage.GetRegistrationResult;
	

});

BankMockApp.controller('postMeController',function($scope,$location,$http,$localStorage){
	$scope.token={};
	var queryValue = getParameterByName('token');
	$scope.token=parseJWT(queryValue);
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function parseJWT(token){

		$http({
			method: 'GET',
			url: '/parseJWT/'+token,
			headers: {'Content-Type': 'application/json'}
		}).then(function(response)
				{			
			console.log("resp->"+response.data);
			$scope.token=angular.toJson(response.data)

				})
				.catch(function(response)
						{
					$scope.error="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
						})	


	}
});


// Navigation between views
BankMockApp.config(['$routeProvider',function($routeProvider){
	$routeProvider
				.when('/getRegistration',{
				templateUrl:'../getRegistration/getregistration.html',
				controller:'RegistrationController'
			})
			.when('/selfEnrollment',{
				templateUrl:'../enrollment/PIIInput.html',
				controller:'selfEnrollmentController'
			})
								
}]);

 