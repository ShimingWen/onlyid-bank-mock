
//Create new Module as onlyDigitalIDApp
var BankMockApp=angular.module('BankMockApp',['ngRoute','ngStorage']);


BankMockApp.controller('loginController',function($scope,$location,$http){
	
	$scope.login={};
	
	$scope.loginAuthentication=function()
	{
		$scope.error='';
		$scope.result='';
		if($scope.login.registrationId==null
				||$scope.login.registrationId==undefined
				||$scope.login.registrationId==""){
			$location.path("/signUp");	
		}
		else{
			$http({
	            method: 'GET',
	            url: '/mockbank/registrations/'+$scope.login.registrationId,
	            headers: {'Content-Type': 'application/json'}
	        }).then(function(response)
					{			
						console.log("resp->"+response.data);
						$scope.result=angular.toJson(response.data)
						localStorage.setItem('GetRegistrationResult', $scope.result)
						$location.path("/getRegistration");	
						
					})
			.catch(function(response)
					{
						$scope.error="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
					})	
			
			
		}
		}
	
});

BankMockApp.controller('selfenrollmentController',function($scope,$location,$http){
	
	//Regex for the UI Input Validation 
	
	$scope.fName_regex = "[a-zA-Z0-9-\' \\\\.&%+\"/]{0,20}";
	$scope.lName_regex = "[a-zA-Z0-9-\' \\\\.&%+\"/]{0,30}";
	$scope.address_regex = "[a-zA-Z0-9#/\',\\\\. ]{0,99}";
	$scope.city_regex = "[a-zA-Z- \'.]{0,30}";
	$scope.zip_regex5 = "[0-9]{5}";
	$scope.zip_regex4 = "^$|[0-9]{4}";
	$scope.state_regex = "^$|[a-zA-Z]{2}";
	$scope.ssn_regex = "[0-9]{4,9}?|^\\s*$";
	$scope.country_regex ="^$|[a-zA-Z]{2}";
	$scope.data={};	
	$scope.data.address={};
	
		
	$scope.registration=function(theForm)
	{
		$scope.errorMes='';
		theForm.$submitted=true;
		console.log(""+$scope.data);
		
		$scope.data.firstName = $scope.selfRegistrationForm.fName;
		$scope.data.middleName = $scope.selfRegistrationForm.mName;
		$scope.data.lastName = $scope.selfRegistrationForm.lName;
		$scope.data.ssn = $scope.selfRegistrationForm.govtNbr;
		$scope.data.address.addressLine1 = $scope.selfRegistrationForm.addressLine1;
		$scope.data.address.addressLine2 = $scope.selfRegistrationForm.addressLine2;
		$scope.data.address.city = $scope.selfRegistrationForm.city;
		$scope.data.address.state = $scope.selfRegistrationForm.state;
		$scope.data.address.zip = $scope.selfRegistrationForm.zipCode5;
		if($scope.selfRegistrationForm.zipCode4 != undefined) {
			$scope.data.address.zip += $scope.selfRegistrationForm.zipCode4;
		}
		$scope.data.country = $scope.selfRegistrationForm.country;
		
		
		
		if(theForm.$valid){
			
			$http.post("/selfRegistration",$scope.data,{ headers: { 'Content-Type': 'application/json' } }).then(function(response)
					{			
				
						console.log("resp->"+response.data.jwtToken);

						document.redirectselfRegistrationForm.elements.id_token.value=response.data.jwtToken;
						document.redirectselfRegistrationForm.action=response.data.portalUrl;		
						document.redirectselfRegistrationForm.method='post';
						document.redirectselfRegistrationForm.submit();
						
					})
			.catch(function(response)
					{
						$scope.errorMes="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
					})
		}
			
		
		
	}
	

});


BankMockApp.controller('signupController',function($scope,$location,$http){

	$scope.data={};
	$scope.data.requestHeader = "";
	$scope.data.requestInputs = "";

	$scope.requestDataJSONOptions = {};
	$scope.requestHeaderJSONOptions = {};
	$scope.token="";
	$http.get('./sampleRequest.json').then(function (response) {
		$scope.requestDataJSONOptions = response.data;
		 $scope.empArray = Object.keys($scope.requestDataJSONOptions)
		     .map(function (value, index) {
		         return { key: value, values: $scope.requestDataJSONOptions[value] };
		     }
		 );
	  });
	
	
	 $scope.showDetails = function () {
		 
        if($scope.requestDataJSON!=null&&$scope.requestDataJSON!=undefined){
    		$scope.data.requestInputs = $scope.requestDataJSON.values;	
    	}
    	else{
    		$scope.data.requestInputs = "";
    	}
    };
    
    $http.get('./headerRequestSample.json').then(function (response) {
		$scope.requestHeaderJSONOptions = response.data;
		 $scope.headerArray = Object.keys($scope.requestHeaderJSONOptions)
		     .map(function (value, index) {
		         return { key: value, values: $scope.requestHeaderJSONOptions[value] };
		     }
		 );
	  });
    
    $scope.showHeader = function () {
    	if($scope.requestHeaderJSON!=null&&$scope.requestHeaderJSON!=undefined){
    		$scope.data.requestHeader = $scope.requestHeaderJSON.values;	
    	}
    	else{
    		$scope.data.requestHeader = "";
    	}
        
    };
	
	$scope.registration=function()
	{
		console.log(""+$scope.data);
		
		$http.post("/mockbank/OnBoarding",$scope.data,{ headers: { 'Content-Type': 'application/json' } }).then(function(response)
				{			
			
					console.log("resp->"+response.data.jwtToken);
					
					localStorage.setItem('returnValue', response.data.mapString);
					localStorage.setItem('transId', response.data.transactionId);
					localStorage.setItem('portalURL', response.data.portalUrl);
					$location.path("/securedPortal");
				})
		.catch(function(response)
				{
					$scope.error="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
				})	
		
		
	}
	$scope.decline=function()
	{
		$location.path("/login");
		
	}
	
	$scope.redirect=function()
	{
		console.log(""+$scope.data);
		
		$http.post("/mockbank/OnBoarding",$scope.data,{ headers: { 'Content-Type': 'application/json' } }).then(function(response)
				{			
					console.log("resp->"+response.data.jwtToken);
			
					$scope.token=response.data.jwtToken;
					
					document.redirectForm.elements.id_token.value=response.data.jwtToken;
					document.redirectForm.action=response.data.portalUrl;		
					document.redirectForm.method='post';
					document.redirectForm.submit();
				})
		.catch(function(response)
				{
					$scope.error="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
				})	
		
		
	}

});



BankMockApp.controller('securedportalController',function($scope,$location,$http,$localStorage){
	
	$scope.jwtJson=localStorage.returnValue;
	$scope.transID=localStorage.transId;
	$scope.portalUrl=localStorage.portalURL;
	

});

BankMockApp.controller('RegistrationController',function($scope,$location,$http,$localStorage){
	
	$scope.results=localStorage.GetRegistrationResult;
	

});

BankMockApp.controller('postMeController',function($scope,$location,$http,$localStorage){
	$scope.token={};
	var queryValue = getParameterByName('token');
	$scope.token=parseJWT(queryValue);
	$location.path("/login");
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function parseJWT(token){
		
		$http({
            method: 'GET',
            url: '/mockbank/parseJWT/'+token,
            headers: {'Content-Type': 'application/json'}
        }).then(function(response)
				{			
					console.log("resp->"+response.data);
					$scope.token=angular.toJson(response.data)
					
				})
		.catch(function(response)
				{
					$scope.error="Error : "+response.data.code +"-"+response.data.message+" ; Transaction Id : "+response.data.transactionId;
				})	
		
		
	}
	});

BankMockApp.controller('verificationController',function($scope,$location,$http,$localStorage){
	$scope.token={};
	var queryValue = getParameterByName('verifCbResp');
	$scope.token=queryValue;
	$location.path("/login");
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
});

// Navigation between views
BankMockApp.config(['$routeProvider',function($routeProvider){
	$routeProvider
			.when('/login',{
				templateUrl:'/mockbank/login/login.html',
				controller:'loginController'
			})
			.when('/signUp',{
				templateUrl:'/mockbank/signUp/signup.html',
				controller:'signupController'
			})
			.when('/securedPortal',{
				templateUrl:'/mockbank/securedPortal/securedportal.html',
				controller:'securedportalController'
			})
				.when('/getRegistration',{
				templateUrl:'/mockbank/getRegistration/getregistration.html',
				controller:'RegistrationController'
			})
			.when('/selfEnrollment',{
				templateUrl:'../enrollment/PIIInput.html',
				controller:'selfenrollmentController'
			})
			.otherwise({
				redirectTo: '/login'
			})
								
}]);

 